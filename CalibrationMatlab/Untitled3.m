newChecker = mask*0+255;
[x, y] = size(checker);
for indx = 1:5:x
    for indy = 1:5:y
        [n,d]=knnsearch(checkerpoints,[indx,indy],'k',4);
        w = d;
        w = (w - min(w)) / ( max(w) - min(w) );
        %if any(w==0)
        %    w = ismember(w,0);
        %end
        
        w = w./(sum(w));
        
        
        newX = int16(indx+ sum(w*(dist(n,1)))); 
        newY = int16(indy+ sum(w*(dist(n,2))));
        
        if newX<=x && newX>=1 && newY<=y && newY >=1
            if checker(indx,indy)>128
                newChecker(newX, newY)=128;
            else
                newChecker(newX, newY)=0;
            end
        end
        
    end
    disp(indx);
end
imshow(newChecker)