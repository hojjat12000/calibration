
%checker = checkerboard(100);
%imshow(checker)

%cpselect(checker, mask)
%mask = rgb2gray(imread('PIcture 2.jpg'));
%maskpoints = fixedPoints2;
%imshow(insertMarker(mask, maskpoints, 'size', 10))
%[n,d]=knnsearch(x,y,'k',10,'distance','minkowski','p',5);

%imshow(insertMarker(checker, checkerpoints(n,:)))


% newChecker = mask*0+255;
% [x, y] = size(checker);
% for indx = 1:5:x
%     for indy = 1:2:y
%         [n,d]=knnsearch(checkerpoints,[indx,indy],'k',4);
%         if any(d==0)
%             d = ismember(d,0);
%         end
%         w = 1./d;
%         w = (w - min(w)) / ( max(w) - min(w) );
%         disp(w)
%         newX = int16(indx- sum(w*(dist(n,1)))); 
%         newY = int16(indy- sum(w*(dist(n,1))));
%         disp([x, y, newX, newY])
%         if newX<=x && newX>=1 && newY<=y && newY >=1 
%             newChecker(newX, newY)=checker(indx,indy);
%         end
%         
%     end
%     disp(indx);
% end
% imshow(newChecker)







% 
% load('calib.mat')
% 
% 
% dist =  checkerPoints - maskPoints;
% 
% % dist(:,1) = dist(:,1) - mean(dist(:,1));
% % dist(:,2) = dist(:,2) - mean(dist(:,2));
% % quiver(checkerPoints(:,1), checkerPoints(:,2), dist(:,1), dist(:,2));
% % dist = -dist;
% % quiver(checkerPoints(:,1), checkerPoints(:,2), dist(:,1), dist(:,2));
% 
% 
% newChecker = mask*0+255;
% newCheckerRGB = [mask;mask;mask];
% newCheckerRGB = newCheckerRGB*0+255;
% newCheckerRGB = reshape(newCheckerRGB, [1024,1280,3]);
% [x, y] = size(checker);
% [x2, y2] = size(mask);
% rgbmask = imread('bs.bmp');
% figcounter = 1;
% figure; hold on;
% for indx = 1:5:x
%     for indy = 1:5:y
%         [n,d]=knnsearch(checkerPoints,[indx,indy],'k',3);
% %         figcounter = figcounter+1;
% %         if(figcounter > 10)
% %             
% %             imshow(insertMarker(insertMarker(checker, checkerPoints(n,:), 'size', 10), [indx, indy]));
% %             figcounter = 1;
% %         end
%         w = d;
%         %w = (w - min(w)) / ( max(w) - min(w) );
%         if any(w==0)
%             w = ismember(w,0);
%         else
%             w = 1./w;
%         end
%                 
%         w = w./(sum(w));
%         
%         
%         newX = int16(indx+ sum(w*(dist(n,1)))); 
%         newY = int16(indy+ sum(w*(dist(n,2))));
%         
%         if newX<=x2 && newX>=1 && newY<=y2 && newY >=1
% %             if checker(indx,indy)>128
% %                 newChecker(newX, newY)=128;
% %             else
% %                 newChecker(newX, newY)=0;
% %             end
%             %newChecker(newX, newY) = checker(indx,indy);
%             newCheckerRGB(newX, newY, :) = rgbmask(indx, indy, :);
%         else
% %             disp('out of wack');
% %             disp([indx, indy]);
% %             disp([newX, newY]);
%         end
%         
%     end
%     disp(indx);
% end
% %imshow(newChecker)
% %imshow(newCheckerRGB);
% 
% imshowpair(mask, newCheckerRGB, 'blend');


%% new checker board
checker = checkerboard(20,16,16); %using a finer checkerboard
imshow(checker);
mask = rgb2gray(imread('Picture 4.jpg'));
imshow(mask);
cpselect(mask, checker);
maskPoints = movingPoints;
checkerPoints = fixedPoints;
save('calib.mat', 'mask','maskPoints','checker','checkerPoints'); %make a backup


%% read from file
filename = 'faceVertices.txt';
delimiterIn = '\t';
headerlinesIn = 1;
A = importdata(filename,delimiterIn);

vertName = A.rowheaders;
verX = A.data(:, 1);
verY = A.data(:, 2);
verZ = A.data(:, 3);



%% calibrate

load('calib.mat') % load the mask, maskPoints, checker, checkerPoints
dist =  checkerPoints - maskPoints; %calculate the distance
dist(:,1) = dist(:,1)-mean(dist(:,1)); %normalize the distance
dist(:,2) = dist(:,2)-mean(dist(:,2)); %normalizing will remove the translation
dist = -dist; % move the vertices in the opposite direction

% the output position holder, for each pixel
newXAr=verX;
newYAr=verY;

k = 3;  %number of neighbors to take into account

%calculate the proportions
width3D = abs(max(verX)-min(verX));
height3D = abs(max(verY)-min(verY));
widthChecker = size(checker,1);
heightChecker = size(checker,2);


scale = -700.0; %this will scale the face so to stop the vertices to go through eachother

for i=1:size(verX)
    %convert vertices to pixels on the checkerboard
    indx = verX(i);
    indx = (indx-min(verX))*widthChecker/width3D;
    indy = verY(i);
    indy = (max(verY)-indy)*heightChecker/height3D;
    
    % this will be used to find how much of the scale should affect each
    % pixel
    deltaX = (indx-widthChecker/2)/widthChecker;
    deltaY = (indy-heightChecker/2)/heightChecker;
    
    [n,d]=knnsearch(checkerPoints,[indx,indy],'k',k); % find the closest points
    
    w = d; % use w to calculate weights
    if any(w==0)
        w = ismember(w,0); %if there is a zero then we are on a exactly point 
    else
        w = 1./w; 
    end
    w = w./(sum(w)); %normalize the weights
    %calculate the new position for each pixel
    newX = scale*deltaX + indx+ sum(w*(dist(n,1))); 
    newY = scale*deltaY+ indy+ sum(w*(dist(n,2)));
    
    %convert each pixel to vertex
    newX = min(verX)+newX*width3D/widthChecker;
    newY = max(verY)-newY*height3D/heightChecker;
    
    %save vertices in an array
    newXAr(i)=newX;
    newYAr(i)=newY;
    
    %progress report
    if mod(i,10)==0
        disp(i);
    end
end


plot3(newXAr, newYAr, verZ, 'k.')

%% save to file
fileID = fopen('newFaceVertices.txt','w');
for i=1:size(verX)
    fprintf(fileID,'%s\t%1.3f\t%1.3f\t%1.3f\n', vertName{i},newXAr(i),newYAr(i),verZ(i));
end
fclose(fileID);
disp('done');
