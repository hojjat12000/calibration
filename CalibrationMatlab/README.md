# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository contains some tools to calibrate a 3D model in Maya (using Matlab) to match the contour of a mask.
* Version 1.0


### How do I get set up? ###

* This codes has been written in MEL (Maya 2016 ), Matlab 2015a.
* You need to show the checkerboard (in the Matlab code) on a mask, take a perfect picture of the mask and import it into Matlab.
* Using `cpselect` in Matlab you need to find the corresponding points in both the checkerboard and the mask picture
* You need to select the vertices on the 3D model face, in Maya, and export them (using the mel code) to a text file to be imported into Matlab.
* In Matlab these vertices will translate and then saved into another text file.
* The new textfile will be imported into Maya (using mel code) anad applied to the vertices.
* Then using (blendshape creator mel code) the new set of blendshapes will be created with the changes applied to them and ready to be exported to Unity.


### Who do I talk to? ###

* Repo owner or admin
